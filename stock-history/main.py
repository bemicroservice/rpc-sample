import os

from services.company_data import CompanyProfile
from flask import Flask

HOST = os.environ.get("HOST")
PORT = os.environ.get("PORT")
MQ_HOST = os.environ.get("MQ_HOST")

app = Flask(__name__)


@app.route("/")
def hello_world():
    """
    Server alive endpoint
    :return:
    """
    return {"Server": "Run"}


@app.route("/company-history/<ticker>")
def company_history(ticker):
    """
    Get company history by ticker
    :param ticker:
    :return:
    """
    company_profile = CompanyProfile(MQ_HOST)
    history = company_profile.get_company_history(ticker)
    return history


@app.route("/company-overview/<ticker>")
def company_overview(ticker):
    """
    Get company overview by ticker
    :param ticker:
    :return:
    """
    company_profile = CompanyProfile(MQ_HOST)
    overview = company_profile.get_company_overview(ticker)
    return overview


if __name__ == '__main__':
    app.run(host=str(HOST), port=int(PORT))