import json
import pika
import time
import logging


class _RemoteMethodResponse:
    """
    RPC response object, that process response and count attempts to insert result
    """
    value = None
    attempts = 0

    def insert_value(self, value: dict):
        self.value = value

    def is_empty(self):
        return self.value is None

    def increase_attempt(self):
        self.attempts = self.attempts + 1

    def result(self) -> dict:
        return self.value


def rabbitmq_rpc_call(mq_host, rpc_queue, message, rpc_call_id, max_attempts=10) -> dict:
    """
    Call method at second service over RabbitMQ queue.
    Use generated callback queue for response delivery.
    The rabbitMQ connection exist only over RPC communication.
    :param mq_host:
    :param rpc_queue:
    :param message:
    :param rpc_call_id:
    :param max_attempts:
    :return:
    """
    with pika.BlockingConnection(pika.ConnectionParameters(mq_host)) as connection:
        channel = connection.channel()
        generate_queue = channel.queue_declare(queue="", exclusive=True)
        callback_queue = generate_queue.method.queue
        response = _RemoteMethodResponse()

        def callback(ch, method, props, body):
            if body is not None:
                try:
                    response.insert_value(json.loads(body))
                except:
                    logging.warning("Wrong format of RPC response")

        channel.basic_consume(
            queue=callback_queue,
            on_message_callback=callback,
            auto_ack=True
        )

        channel.basic_publish(
            exchange="",
            routing_key=rpc_queue,
            properties=pika.BasicProperties(reply_to=callback_queue, correlation_id=rpc_call_id),
            body=message
        )

        while response.is_empty() and response.attempts <= max_attempts:
            connection.process_data_events()
            response.increase_attempt()
            time.sleep(0.1)

        connection.close()

        return response.result()
