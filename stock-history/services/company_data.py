import json
import uuid
from services.message_queue import rabbitmq_rpc_call
from dataclasses import dataclass
import logging


@dataclass
class CompanyProfile:
    mq_host: str

    def get_company_history(self, ticker: str) -> dict:
        """
        Inner DB method, that get data about stock price history of the ticker.
        :param ticker:
        :return:
        """
        try:
            with open(f"fixtures/{ticker.lower()}-history.json") as file:
                raw_data = json.load(file)
                history_data = raw_data["Time Series (Daily)"]
                return history_data

        except FileExistsError:
            logging.critical("Load data-file error")
            return dict()

    def get_company_overview(self, ticker: str) -> dict:
        """
        RPC method, that call other service and get company overview data for response
        :param ticker:
        :return:
        """
        try:
            message = json.dumps({"rpc_method": "getSingleCompany", "params": {"ticker": ticker}})
            rpc_result = rabbitmq_rpc_call(
                mq_host=self.mq_host,
                rpc_queue="rpc_queue",
                rpc_call_id=str(uuid.uuid4()),
                message=message)

            return rpc_result["data"] if rpc_result["status"] == "ok" else dict()
        except:
            logging.critical("MQ Connection Error")
            return dict()
