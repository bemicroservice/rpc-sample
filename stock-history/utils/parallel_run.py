from typing import Callable
import threading


def run_in_daemon(job: Callable):
    def decorator_wrap():
        th = threading.Thread(target=job)
        th.daemon = True
        th.start()
        return th.getName()

    return decorator_wrap


def run_in_loop(job: Callable):
    def decorator_wrap():
        while True:
            job()

    return decorator_wrap
