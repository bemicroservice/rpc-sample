import filesystem from "fs";
import { DatabaseResource } from "../models/sampleModels";

export default class CompanyProfileDB implements DatabaseResource {

  /**
   * Gets overview data for specific company from storage
   * @param ticker 
   * @returns 
   */
  getSingleCompany(ticker: string){
    const storageContent = this.readFile(ticker)
    return storageContent
  }

  private readFile(ticker: string){
    try {
      return JSON.parse(filesystem.readFileSync(`./fixtures/${ticker.toLowerCase()}-overview.json`).toString())
    } catch (error) {
      console.error(error)
      return undefined
    }
  }
}