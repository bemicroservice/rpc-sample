import RabbitMQ from "amqplib";
import pino from "pino";
import { RpcInputMessage, RpcOutputMessage } from "../models/sampleModels";
import CompanyProfileDB from "./company-profile";

export interface RpcServerProps {
  host: string,
  logger: pino.Logger,
  db: CompanyProfileDB
}

/**
 * Start RPC server over RabbitMQ using single queue for RPC requests and 
 * client-delivered queues for responses.
 * @param props 
 * @returns 
 */
export const RpcRabbitMqServer = async (props: RpcServerProps) => {

  const { host, logger, db } = props
  const RPC_QUEUE = "rpc_queue"

  try {
    let rmqConnection: RabbitMQ.Connection = await RabbitMQ.connect(host)
    let rmqChannel: RabbitMQ.Channel = await rmqConnection.createChannel()

    logger.info("RabbitMQ Connected")

    rmqChannel.assertQueue(RPC_QUEUE)
    rmqChannel.prefetch(1)
    rmqChannel.consume(RPC_QUEUE, (messageEvent) => {

      if (messageEvent) {
        try {
          logger.info("New RPC request")

          const inputMessage: RpcInputMessage = JSON.parse(messageEvent.content.toString())
          const outputMessage: RpcOutputMessage = processRpcResponse(inputMessage)

          rmqChannel.sendToQueue(messageEvent.properties.replyTo, Buffer.from(JSON.stringify(outputMessage)),
            { correlationId: messageEvent.properties.correlationId }
          )
        } catch (error) {
          logger.warn("Wrong message format")
        }
        finally {
          rmqChannel.ack(messageEvent)
        }
      }
    })

    /**
     * Process RPC request by type - what inner method will process the request.
     * @param incomeMessage 
     * @returns 
     */
    const processRpcResponse = (incomeMessage: RpcInputMessage): RpcOutputMessage => {
      if (incomeMessage.rpc_method === "getSingleCompany")
        return { status: "ok", data: db.getSingleCompany(incomeMessage.params["ticker"]) }
      else
        return { status: "fail" }
    }

    return rmqConnection
  } catch (error) {
    //logger.error(error)
    await new Promise(resolve => setTimeout(resolve, 5000))
    await RpcRabbitMqServer(props)
  }
}