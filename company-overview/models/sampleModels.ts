export interface DatabaseResource{
  getSingleCompany: (ticker: string) => any
}

export interface RpcInputMessage{
  rpc_method: string,
  params: any
}

export interface RpcOutputMessage{
  status: string,
  data?: any
}