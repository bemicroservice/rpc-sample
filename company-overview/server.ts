import PinoLogger from "pino";
import CompanyProfileDB from "./services/company-profile";
import { RpcRabbitMqServer } from "./services/rabbitmq-rpc-server";


const RABBITMQ_HOST = process.env.MQ_HOST as string

//start server
Promise.resolve(RpcRabbitMqServer({
  host: RABBITMQ_HOST,
  logger: PinoLogger(),
  db: new CompanyProfileDB()
}))
