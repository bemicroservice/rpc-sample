import { expect } from "chai"
import "mocha"

import CompanyProfile from "../services/company-profile"

describe("Load company by ticker", () => {

  it("Read content from file", () => {
    const readerInstance = new CompanyProfile()
    const tickers : Array<[string, boolean]> = [
      ["AAPL", false],
      ["aapl", false], 
      ["AaPl", false],
      ["dodo", true]
    ]
    
    for (let index = 0; index < tickers.length; index++) {
      const [tick, shudBeUndefined] = tickers[index]
      const content = readerInstance.getSingleCompany(tick)
      
      if(shudBeUndefined)
        expect(content).to.be.undefined
      else
        expect(content).not.to.be.undefined
    }
  })

})